import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  ScrollView,
  View,
  TouchableOpacity,
} from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import "react-native-gesture-handler";

function HomeScreen() {
  const [todoList, setTodoList] = useState([]);
  console.log(`To Do List Updated: ${todoList}`);

  const navigation = useNavigation();

  // handler functions
  const handleAddTodo = value => {
    console.log(`Adding To Do: ${value.text} time: ${value.date}`);
    setTodoList(list => [...list, value]);
  };

  const handleRemoveTodo = idx => {
    console.log(`Removing To Do: ${todoList[idx].text}`);

    setTodoList(list => {
      const arr = [...list];
      arr.splice(idx, 1);
      return arr;
    });
  };

  const dateNow = new Date();

  return (
    <View style={styles.wrapper}>
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.headline}>To-Do List</Text>
        <View style={styles.todoContainer}>
          {todoList.map((todo, idx) => {
            const todoItemStyle = [styles.todoItem];
            const timeLeft = todo.date - dateNow;
            if (timeLeft < 14400000 && timeLeft > 0)
              todoItemStyle.push(styles.closeTodoItem);
            return (
              <TouchableOpacity
                key={idx}
                onPress={() => {
                  console.log(`Clicked on todo ${idx}.`);
                  navigation.push("TodoView", { todo });
                }}
              >
                <View style={todoItemStyle}>
                  <Text style={styles.todoText}>{todo.text}</Text>
                  <TouchableOpacity
                    style={[styles.button, styles.doneButton]}
                    onPress={() => handleRemoveTodo(idx)}
                  >
                    <Text style={styles.doneButtonText}>Mark as Done</Text>
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            );
          })}

          {!todoList.length && (
            <View style={styles.noContent}>
              <Text style={styles.noContentText}>No to do items yet.</Text>
            </View>
          )}
        </View>
      </ScrollView>
      <TouchableOpacity
        style={[styles.button, styles.addButton]}
        onPress={() =>
          navigation.push("AddTodo", {
            handleAddTodo,
          })
        }
      >
        <Text style={styles.buttonText}>Add To Do</Text>
      </TouchableOpacity>
    </View>
  );
}

function AddTodoScreen({ route }) {
  const { params } = route;
  const { handleAddTodo } = params;

  const dateToday = new Date();
  dateToday.setHours(0, 0, 0, 0);

  const [value, setValue] = useState("");
  const [date, setDate] = useState(dateToday);
  const [mode, setMode] = useState("date");
  const [showPicker, setShowPicker] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setShowPicker(false);
    setDate(currentDate);
  };

  const navigation = useNavigation();

  return (
    <View style={styles.wrapper}>
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.headline}>Add To Do</Text>
        <TextInput
          placeholder="Enter new task here..."
          style={styles.input}
          onChangeText={text => setValue(text)}
          value={value}
        />
        <View style={styles.dateContainer}>
          <Text style={styles.dateText}>
            {date?.toLocaleDateString() || "-"}
          </Text>
          <TouchableOpacity
            style={[styles.button, styles.showDateButton]}
            onPress={() => {
              setMode("date");
              setShowPicker(true);
            }}
          >
            <Text style={styles.buttonText}>Select a Date</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.timeContainer}>
          <Text style={styles.dateText}>
            {date?.toLocaleTimeString() || "-"}
          </Text>
          <TouchableOpacity
            style={[styles.button, styles.showDateButton]}
            onPress={() => {
              setMode("time");
              setShowPicker(true);
            }}
          >
            <Text style={styles.buttonText}>Select a Time</Text>
          </TouchableOpacity>
        </View>
        {showPicker && (
          <DateTimePicker
            value={date}
            mode={mode}
            is24Hour={true}
            onChange={onChange}
          />
        )}
        <TouchableOpacity
          onPress={() => {
            handleAddTodo({ text: value, date: date });

            navigation.goBack();
          }}
          style={[styles.button, styles.addButton]}
        >
          <Text style={styles.buttonText}>Add To Do</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

function TodoViewScreen({ route }) {
  const { todo } = route.params;
  const timeLeft = todo.date - new Date();
  const [timer, setTimer] = useState(timeLeft);

  const displayTimer = origtimestamp => {
    const timestamp = Math.floor(origtimestamp / 1000);
    const hours = Math.floor(timestamp / 3600);
    const minutes = Math.floor((timestamp - 3600 * hours) / 60);
    const seconds = timestamp - 3600 * hours - 60 * minutes;

    return `${String(hours).padStart(2, "0")}:${String(minutes).padStart(
      2,
      "0",
    )}:${String(seconds).padStart(2, "0")}`;
  };

  useEffect(() => {
    let interval = setInterval(() => {
      setTimer(lastTimerCount => {
        lastTimerCount <= 1000 && clearInterval(interval);
        return lastTimerCount - 1000;
      });
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <View style={styles.wrapper}>
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.headline}>{todo.text}</Text>
        <Text style={styles.bodyText}>
          Deadline: {todo.date.toLocaleString()}
        </Text>
        {timeLeft < 14400000 && timeLeft > 0 && (
          <View style={styles.deadlineContainer}>
            <Text style={styles.deadlineText}>Deadline is close!</Text>
            <Text style={styles.deadlineText}>{displayTimer(timer)}</Text>
          </View>
        )}
      </ScrollView>
    </View>
  );
}

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen
          name="AddTodo"
          component={AddTodoScreen}
          options={{ title: "Add To Do" }}
        />
        <Stack.Screen
          name="TodoView"
          component={TodoViewScreen}
          options={{ title: "Todo Details" }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    height: "100%",
    width: "100%",
    backgroundColor: "#fff",
  },
  container: {
    flex: 1,
    width: "100%",
    padding: 30,
    alignItems: "stretch",
  },
  headline: {
    fontSize: 30,
    marginBottom: 15,
  },
  bodyText: {
    fontSize: 18,
  },
  todoContainer: {
    flexDirection: "column",
    flex: 1,
  },
  deadlineContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginVertical: 10,
    backgroundColor: "#cc3300",
    borderRadius: 10,
  },
  deadlineText: {
    fontSize: 20,
    color: "white",
  },
  todoItem: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    backgroundColor: "white",
    alignItems: "center",
    marginVertical: 10,
    borderRadius: 15,
    flexDirection: "row",
    width: "100%",
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  closeTodoItem: {
    backgroundColor: "#efc1b2",
  },
  todoText: {
    marginRight: 15,
  },
  dateContainer: {
    backgroundColor: "white",
    alignItems: "center",
    marginVertical: 10,
    borderRadius: 15,
    flexDirection: "row",
    width: "100%",
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  timeContainer: {
    backgroundColor: "white",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 0,
    borderRadius: 15,
    flexDirection: "row",
    width: "100%",
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  doneButton: {
    marginLeft: "auto",
    margin: 0,
    borderRadius: 30,
    paddingHorizontal: 12,
    paddingVertical: 8,
  },
  doneButtonText: {
    color: "white",
  },
  todoText: {
    fontSize: 16,
    flex: 1,
  },
  noContent: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  noContentText: {
    opacity: 0.5,
    alignSelf: "center",
    textAlign: "center",
  },
  input: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderColor: "#bebebe",
    borderRadius: 0,
    borderWidth: 1,
    width: "100%",
  },
  button: {
    backgroundColor: "#7e7e7e",
    alignSelf: "center",
  },
  addButton: {
    borderRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 8,
    backgroundColor: "#7cc767",
    margin: 30,
  },
  buttonText: {
    color: "white",
    fontSize: 20,
  },
  showDateButton: {
    borderRadius: 30,
    marginLeft: "auto",
    margin: 0,
    borderRadius: 30,
    paddingHorizontal: 12,
    paddingVertical: 8,
    backgroundColor: "#7cc767",
  },
  dateText: {
    color: "black",
    fontSize: 15,
    width: "100%",
  },
});
